//COSMOS libraries
#include "support/configCosmos.h"
#include "support/elapsedtime.h"
#include "support/timeutils.h"
#include "device/serial/serialclass.h"
#include "agent/agentclass.h"

//mavlink library
#include <mavlink.h>

#include <iostream>
#include <string>
using namespace std;

int myagent();

string nodename = "uhabs5";
string agentname = "aerocore";

int waitsec = 1; // wait to find other agents of your 'type/name', seconds
int loopmsec = 1; // period of heartbeat
char buf4[512];

beatstruc beat_agent_002;
Agent *agent; // to access the cosmos data, will change later

#define MAXBUFFERSIZE 256 // comm buffer for agents

int main(int argc, char *argv[])
{
  cout << "Starting agent " << endl;

  //Create agent
  agent = new Agent(nodename, agentname);

//  string sohtemp = json_list_of_soh(agent->cinfo).c_str();
//  printf("Devices: %s\n", sohtemp.c_str());

  // Set SOH String
  char agent_aerocore_soh[2000] = "{\"device_tsen_temp_000\",\"device_ssen_elevation_000\",\"device_imu_accel_000\",\"device_gps_dgeocv_000\"}";
  agent->set_sohstring(agent_aerocore_soh);

  myagent();

  return 0;
}

int myagent()
{
    cout << "agent " << agentname <<  " ...online " << endl;

    //Initialize the required buffers
    mavlink_message_t msg;
    mavlink_status_t status;
    //uint8_t buf[MAVLINK_MAX_PACKET_LEN];
    //uint16_t len;
    //int bytes_sent;

    // Example variable, by declaring them static they're persistent
    // and will thus track the system state
    static int packet_drops = 0;
    //static int mode = 0; //Defined in mavlink_types.h, which is included by mavlink.h

    cout << "Open serial port /dev/ttyO1, status: " ;
    Serial serial("/dev/ttyO1", 115200, 8, 0, 1);
    cout << serial.get_error() << endl;

    //declare variables
    float baro;
    float alti;
    float tin;
    float xacc;
    float yacc;
    float zacc;
    int32_t lat;
    int32_t lon;

    while(agent->running())
      {
        uint8_t c = serial.get_char();
        //cout << c << endl;
        //Try to get a new message
        if(mavlink_parse_char(MAVLINK_COMM_1, c, &msg, &status))
          {
            // Handle message
            printf("Received packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d", msg.sysid, msg.compid, msg.len, msg.msgid);
            cout << endl;
            // 74 - VRF_HUD
            // 87 - POSITION_TARGET_GLOBAL_INT
            // 0 - HEARTBEAT
            // 1 - SYS_STATUS
            // 105 - HIGHRES_IMU

            switch(msg.msgid)
              {
                case MAVLINK_MSG_ID_HIGHRES_IMU:
                mavlink_highres_imu_t imu;
                mavlink_msg_highres_imu_decode(&msg, &imu);

                /*
                printf("Got message HIGHRES_IMU");
                printf("\t time: %llu\n", imu.time_usec);
                printf("\t acc  (NED):\t% f\t% f\t% f (m/s^2)\n", imu.xacc, imu.yacc, imu.zacc);
                printf("\t gyro (NED):\t% f\t% f\t% f (rad/s)\n", imu.xgyro, imu.ygyro, imu.zgyro);
                printf("\t mag  (NED):\t% f\t% f\t% f (Ga)\n", imu.xmag, imu.ymag, imu.zmag);
                printf("\t baro: \t %f (mBar)\n", imu.abs_pressure);
                printf("\t altitude: \t %f (m)\n", imu.pressure_alt);
                printf("\t temperature: \t %f C\n", imu.temperature);
                printf("\n");
                cout << endl;
                */

                baro = imu.abs_pressure;
                alti = imu.pressure_alt;
                tin = imu.temperature;
                xacc = imu.xacc;
                yacc = imu.yacc;
                zacc = imu.zacc;
                break;

//                case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT:
//                mavlink_position_target_global_int_t pos;
//                mavlink_msg_gps2_raw_get_lat(msg);
//                printf("Got message POSITION");
//                printf("\t time: %llu\n", imu.time_usec);
//                printf("\t lat:\t %d\n", pos.lat_int);
//                printf("\t lon:\t %d\n", pos.lon_int);
//                printf("\n");

                case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
                mavlink_global_position_int_t pos;
                lat = pos.lat;
                lon = pos.lon;

                break;

                default:
                //Do nothing

                break;
              }
          }

        //assign value
        //agent->cinfo->pdata.node.state = temp;
        agent->cinfo->devspec.gps[0]->dgeocv.col[0] = lon;
        agent->cinfo->devspec.gps[0]->dgeocv.col[1] = lat;
        agent->cinfo->devspec.tsen[0]->temp = tin;
        agent->cinfo->devspec.imu[0]->accel.col[0] = xacc;
        agent->cinfo->devspec.imu[0]->accel.col[1] = yacc;
        agent->cinfo->devspec.imu[0]->accel.col[2] = zacc;
        agent->cinfo->devspec.ssen[0]->elevation = alti;

//        cout << tin << endl;
        //And get the next one
        //COSMOS_SLEEP(1);
      }
    //Update global packet drops counter
    packet_drops += status.packet_rx_drop_count;

    return (0);

}
